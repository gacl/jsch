package com.itgacl.jsch.test;

import com.itgacl.jsch.SftpFileService;
import com.itgacl.jsch.bean.SftpUploadResult;
import com.itgacl.jsch.util.FileUtil;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by gacl on 2017/10/16.
 */
public class SftpFileServiceTest {

    private static Logger logger = Logger.getLogger(SftpFileServiceTest.class);

    private static SftpFileService fileService;

    /**
     * 针对所有测试，只执行一次，且必须为static void
     */
    @BeforeClass
    public static void init() {
        fileService = new SftpFileService("sFtpConfig.properties");
    }

    @Test
    public void testUploadFile1(){
        try {
            FileInputStream fileInputStream = new FileInputStream("C:\\Users\\29060\\Desktop\\TB\\smart-metering-model.png");
            SftpUploadResult uploadResult = fileService.uploadFile(fileInputStream, "电脑配置.jpg");
            logger.debug("uploadResult="+uploadResult);
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage(),e);
        }
    }

    @Test
    public void testUploadFile2(){
        try {
            FileInputStream fileInputStream = new FileInputStream("D:\\测试数据\\4.png");
            SftpUploadResult uploadResult = fileService.uploadFile(fileInputStream, "4.png","gaclXDP");
            logger.debug("uploadResult="+uploadResult);
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage(),e);
        }
    }

    @Test
    public void testUploadFile3(){
        try {
            File file = new File("D:/测试数据/me.jpg");
            SftpUploadResult uploadResult = fileService.uploadFile(file, "gacl/test");
            logger.debug("uploadResult="+uploadResult);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
    }

    @Test
    public void testUploadFile4(){
        try {
            FileInputStream fileInputStream = new FileInputStream("D:\\测试数据\\4.png");
            SftpUploadResult uploadResult = fileService.uploadFile(FileUtil.input2ByteArray(fileInputStream), "4.png");
            logger.debug("uploadResult="+uploadResult);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
    }

    @Test
    public void testUploadFile5(){
        try {
            FileInputStream fileInputStream = new FileInputStream("D:\\测试数据\\Cg2BwVmv-oCAPA91AAJ6fgJZatg503.jpg");
            SftpUploadResult uploadResult = fileService.uploadFile(FileUtil.input2ByteArray(fileInputStream), "Cg2BwVmv-oCAPA91AAJ6fgJZatg503.jpg","gacl/test");
            logger.debug("uploadResult="+uploadResult);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
    }

    @Test
    public void testUploadFile6(){
        try {
            SftpUploadResult uploadResult = fileService.uploadFile("D:\\测试数据\\4.png");
            logger.debug("uploadResult="+uploadResult);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
    }

    @Test
    public void testUploadFile7(){
        try {
            SftpUploadResult uploadResult = fileService.uploadFile("D:/测试数据/Cg2BwVmv-oCAPA91AAJ6fgJZatg503.jpg","/gacl/test/xdp/test");
            logger.debug("uploadResult="+uploadResult);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
    }

    @Test
    public void testUploadNetworkFile1(){
        try {
            SftpUploadResult uploadResult = fileService.uploadNetworkFile("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1508751925&di=d23ac8ad28fb2046936f47f991b7e2ca&imgtype=jpg&er=1&src=http%3A%2F%2Fwww.gwchina.cn%2Fuploadfile%2F2016%2F0804%2F20160804015824783.jpg");
            logger.debug("uploadResult="+uploadResult);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
    }

    @Test
    public void testUploadNetworkFile2(){
        try {
            SftpUploadResult uploadResult = fileService.uploadNetworkFile("http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLJ9jRgnPbNfJGZThlDNNvNd8ulqGhQO34tL82PZEVWGzG4OHIWtM2xazcAnjpZzZ3xLfeEic4cvIg/0","png");
            logger.debug("uploadResult="+uploadResult);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
    }

    /**
     * uploadResult=UploadResult{originalFileName='4.png', newFileName='9C6A4F38E6166A6CF0EB36BD326D1D0B.png', savePath='/mydata/app1/uploadFilesManager/9C6A4F38E6166A6CF0EB36BD326D1D0B.png', fileUrl='http://10.1.1.31/uploadFilesManager/9C6A4F38E6166A6CF0EB36BD326D1D0B.png', fileSuffix='png', storageType='SFTP', isUploadSuccess=true, descDetail='null', useTime=36, extFieldMap={remoteSavePath=/mydata/app1/uploadFilesManager}}
     * uploadResult=UploadResult{originalFileName='Cg2BwVmv-oCAPA91AAJ6fgJZatg503.jpg', newFileName='4F2D9F19813CA90B22C6EC7EA769D2EB.jpg', savePath='/mydata/app1/uploadFilesManager/gacl/test/4F2D9F19813CA90B22C6EC7EA769D2EB.jpg', fileUrl='http://10.1.1.31/uploadFilesManager/gacl/test/4F2D9F19813CA90B22C6EC7EA769D2EB.jpg', fileSuffix='jpg', storageType='SFTP', isUploadSuccess=true, descDetail='null', useTime=37, extFieldMap={remoteSavePath=/mydata/app1/uploadFilesManager/gacl/test}}
     */
    @Test
    public void testDeleteFile1() {
        //fileService.deleteFile("/mydata/app1/uploadFilesManager","9C6A4F38E6166A6CF0EB36BD326D1D0B.png");
        int result = fileService.deleteFile("/mydata/app1/uploadFilesManager/gacl/test", "4F2D9F19813CA90B22C6EC7EA769D2EB.jpg");
        logger.debug("result=" + result);
    }

    /**
     * uploadResult=UploadResult{originalFileName='0.png', newFileName='26323A44A1CA5DF0761769A2A6B6A4A9.png', savePath='/mydata/app1/uploadFilesManager/26323A44A1CA5DF0761769A2A6B6A4A9.png', fileUrl='http://192.168.1.107/uploadFilesManager/26323A44A1CA5DF0761769A2A6B6A4A9.png', fileSuffix='png', storageType='SFTP', isUploadSuccess=true, descDetail='null', useTime=1545, extFieldMap={remoteSavePath=/mydata/app1/uploadFilesManager}}
     */
    @Test
    public void testDeleteFile2() {
        //fileService.deleteFile("/mydata/app1/uploadFilesManager","9C6A4F38E6166A6CF0EB36BD326D1D0B.png");
        int result = fileService.deleteFile("/mydata/app1/uploadFilesManager/26323A44A1CA5DF0761769A2A6B6A4A9.png");
        logger.debug("result=" + result);
    }

    @Test
    public void testBatchDeleteFile1(){
        List<String> fileNameList = new ArrayList<>();
        //http://10.1.1.31/uploadFilesManager/gacl/test/3F5DA97DE423DAD861AEC9CD33769FC7.jpg
        fileNameList.add("3F5DA97DE423DAD861AEC9CD33769FC7.jpg");
        //http://10.1.1.31/uploadFilesManager/gacl/test/A7C167DB9EFFC1E2E80A09E7F8AB6F36.jpg
        fileNameList.add("A7C167DB9EFFC1E2E80A09E7F8AB6F36.jpg");
        //fileService.deleteFile("/mydata/app1/uploadFilesManager","9C6A4F38E6166A6CF0EB36BD326D1D0B.png");
        Map<String,Integer> resultMap = fileService.batchDeleteFile("/mydata/app1/uploadFilesManager/gacl/test",fileNameList);
        logger.debug("resultMap="+resultMap);
    }

    /**
     * 测试通过
     */
    @Test
    public void testDownloadFile1(){
        byte[] fileByteArr = fileService.downloadAsByte("/mydata/app1/uploadFilesManager/DC670DD6E0BA11AD5F4A9AE7516674F0.jpg");
        if(fileByteArr != null){
            try {
                FileUtil.saveFile("E:/测试数据/logo1.jpg", fileByteArr);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void testDownloadFile2(){
        fileService.downloadFile("/mydata/app1/uploadFilesManager/gacl/test","053089E5983411B132CF4061F6FDA7C4.jpg","D:/测试数据/logo.jpg");
    }

    @Test
    public void testDownloadFile3(){
        byte[] fileByteArr = fileService.downloadFile("http://10.1.1.31/uploadFilesManager/5469F1F1C9B276EB845A5B1E312EE898.png");
        if(fileByteArr != null){
            try {
                FileUtil.saveFile("D:/测试数据/head.jpg",fileByteArr);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @Test
    public void testFormatPath() {
        List<String> list = FileUtil.formatPath("/mydata/app1/uploadFilesManager/gacl/test/C5C5E1A94ECF4CA72A2B3566E8215022.jpg");
        logger.debug("list=" + list);
    }

    @Test
    public void testDownloadAsFile() {
        File downloadFile = fileService.downloadAsFile("/mydata/app1/uploadFilesManager/DC670DD6E0BA11AD5F4A9AE7516674F0.jpg", "E:/测试数据/XDP/GACL/GACL.jpg");
        logger.debug("downloadFile=" + downloadFile);
    }
}

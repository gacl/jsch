package com.itgacl.jsch;

/**
 * Created by gacl on 2017/10/16.
 */
public class SftpConfig {

    /**
     * 服务器SSH连接IP地址
     */
    private String serverIp;

    /**
     * 服务器SSH连接端口号
     */
    private int port = 22;

    /**
     * 服务器登录用户名
     */
    private String userName;

    /**
     * 服务器登录密码
     */
    private String password;

    /**
     * SSH 客户端的 StrictHostKeyChecking 配置成no后，可以实现当第一次连接服务器时，自动接受新的公钥
     */
    private String strictHostKeyChecking = "no";

    /**
     * 存放上传文件根目录
     */
    private String workPath;

    /**
     * 文件服务器域名
     */
    private String fileServerDomain;

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStrictHostKeyChecking() {
        return strictHostKeyChecking;
    }

    public void setStrictHostKeyChecking(String strictHostKeyChecking) {
        this.strictHostKeyChecking = strictHostKeyChecking;
    }

    public String getWorkPath() {
        return workPath;
    }

    public void setWorkPath(String workPath) {
        this.workPath = workPath;
    }

    public String getFileServerDomain() {
        return fileServerDomain;
    }

    public void setFileServerDomain(String fileServerDomain) {
        this.fileServerDomain = fileServerDomain;
    }
}

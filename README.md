# 基于对JSch - Java Secure Channel进行的封装，实现java远程操作文件服务器(linux)，使用sftp协议，linux服务器不用安装任何插件和软件

sftp是Secure File Transfer Protocol的缩写，安全文件传送协议。可以为传输文件提供一种安全的加密方法。sftp 与 ftp 有着几乎一样的语法和功能。
SFTP 为 SSH的一部分，是一种传输档案至 Blogger 伺服器的安全方式。其实在SSH软件包中，已经包含了一个叫作SFTP(Secure File Transfer Protocol)的安全文件传输子系统，
SFTP本身没有单独的守护进程，它必须使用sshd守护进程（端口号默认是22）来完成相应的连接操作，所以从某种意义上来说，SFTP并不像一个服务器程序，而更像是一个客户端程序。
SFTP同样是使用加密传输认证信息和传输的数据，所以，使用SFTP是非常安全的。但是，由于这种传输方式使用了加密/解密技术，所以传输效率比普通的FTP要低得多，
如果对网络安全性要求更高时，可以使用SFTP代替FTP。

## jsch
Jsch是纯Java实现的SSH开源框架，可以用来执行shell命令，实现sftp上传
官网：http://www.jcraft.com/jsch/

## 发布到nexus release仓库
mvn deploy -P release

## 发布到nexus snapshots仓库
mvn deploy

## 将本地的仓库和远程的仓库进行关联
git remote add origin https://gitee.com/gacl/jsch.git

## Maven 结合 IDEA 入门实践
http://www.cnblogs.com/ramantic/p/7735323.html

